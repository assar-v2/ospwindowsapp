ASSAR PROJECT VARIANCES
=======================
1. TO COVER 'SA' AMOUNT < 1000 	ADJUST TO 'IU'
2. TO COVER 'TR' AMOUNT + 	ADJUST TO 'IU'
3. TO COVER 'TR' DOB < 55YR 	ADJUST TO 'RD'
4. TO COVER DUPLICATION PRICE HISTORY DATE
5. TO COVER MISSING PRICE HISTORY
6. TO PATCH RPT 18 BASED RPT 11 (WITHOUT RELEASE MEMBER)


Pre-process preparation
=======================
1.	Copy and paste UTMC05 - 09 data file into import data folder for report services 
2.	Restore the latest UTS into report server 
3. 	Choose the date and start the report services

re-run the report month
=======================
4.	run the query 18SQL-SQL7.sql to purge the previous data



Process Sequence
================
1==	Import fund level report with following sequence
		==> rpt 16, 18, 12, 17
	Import member level report with following sequence
		==> rpt 15, 11, 10, 14, 13, 03
2==	Export UTS UTMC rpt (03, 10, 11, 12, 13, 14, 15, 16, 17, && 18) to apex_export folder
3==	Copy UTS UTMC rpt to OSP folder
	Import UTMC 17 rpt && rpt 2


10.10.10.141 oracle kl sentral
192.168.0.109 enjoy08

Changed
-------
1-	Output file based on 1 month 1 file

Indexes
-------
CREATE UNIQUE INDEX monthly_member_investment_by_fund

	ON utmc_member_investment (IPD_Fund_Code, IPD_Member_Acc_No, Report_Date);



CREATE UNIQUE INDEX monthly_member_compositional_investment_by_fund

	ON utmc_compositional_investment (IPD_Fund_Code, IPD_Member_Acc_No, Report_Date);




CREATE UNIQUE INDEX monthly_utmc_member_information 
	ON utmc_member_information (Report_Date, EPF_No, ID);




CREATE UNIQUE INDEX monthly_utmc_compositional_transactions  
	ON utmc_compositional_transactions (Report_Date, Transaction_Code, 

Date_Of_Transaction, IPD_Member_Acc_No, ID)

CREATE UNIQUE INDEX monthly_utmc_transferred  
	ON utmc_transferred (Report_Date, IPD_Member_Acc_No, IPD_Fund_Code, ID);




CREATE UNIQUE INDEX monthly_utmc_release_control  
	ON utmc_release_control (Report_Date, IPD_Member_Acc_No, 

IPD_Fund_Code, ID)

	FUNDLEVEL
	=========
CREATE UNIQUE INDEX Idx_utmc_fund_daily_nav 
	
	ON utmc_fund_daily_nav (Report_Date, IPD_Fund_Code, NAV_Date_Daily, ID);




CREATE UNIQUE INDEX Idx_utmc_daily_nav_fund
 
	ON utmc_daily_nav_fund (Report_Date, IPD_Fund_Code, Daily_NAV_Date, ID)



CREATE UNIQUE INDEX Idx_utmc_daily_nav
 
	ON utmc_daily_nav (Report_Date, Daily_NAV_Date)


	//PMS
CREATE UNIQUE INDEX Mth_utmc_fund_sector_composition
 
	ON utmc_fund_sector_composition(Report_Date, IPD_Fund_Code, ID)



CREATE UNIQUE INDEX Mth_utmc_fund_asset_class_composition

 
	ON utmc_fund_asset_class_composition
(Report_Date, IPD_Fund_Code, ID)

CREATE UNIQUE INDEX Mth_utmc_fund_shariah_composition
 
	ON utmc_fund_shariah_composition(Report_Date, IPD_Fund_Code, ID)



CREATE UNIQUE INDEX Mth_utmc_utmc_fund_country_composition

 
	ON utmc_fund_country_composition
(Report_Date, IPD_Fund_Code, ID)

CREATE UNIQUE INDEX Mth_utmc_fund_currency_composition

 
	ON utmc_fund_currency_composition
(Report_Date, IPD_Fund_Code, ID)



PMS Level
---------
select * from utmc_fund_asset_class_composition
where  

select * from utmc_fund_sector_composition
select * from 

utmc_fund_shariah_composition
select * from 

utmc_fund_country_composition


select * from 

utmc_fund_currency_composition



fund level
----------

Select * from utmc_fund_daily_nav where Report_Date = '2016-09-30';


Select * from utmc_daily_nav_fund where Report_Date = '2016-09-30';


Select * from utmc_daily_nav where Report_Date = '2016-09-30';


Select * from utmc_fund_corporate_actions where Report_Date = '2016-09-30';




delete from utmc_fund_asset_class_composition;



delete from utmc_fund_sector_composition;


Delete from utmc_fund_shariah_composition;


Delete from utmc_fund_country_composition;


Delete from utmc_fund_currency_composition;


member level
------------

Select * from utmc_member_information where Report_Date = '2016-09-30';

Select * from utmc_compositional_transactions where Report_Date = '2016-09-30';

Select * from utmc_compositional_investment where Report_Date = '2016-09-30';

Select * from utmc_transferred where Report_Date = '2016-09-30';

Select * from utmc_release_control where Report_Date = '2016-09-30';

Select * from utmc_member_investment where Report_Date = '2016-09-30';



Adjustment (Hardcode)
----------
1 - 11667355 adjust to ==> "S" (decease case)	
'10402564', '61272095', '17532738', '15451436', '05428762', '13581257', '16028600', '04159001', '10680005', '15286233', '13621082', '14901511', '10142244', '13825195', '10209020',
'13430327', '10116499', '13644892', '10510752', '13938394', '12180650', '05265633', '10933506', '13267496', '14486547', '11399640', '16837490', '11040896', '10420784', '16080296',
'05249109', '11690534', '11105589', '11587384', '10855509', '11656335', '11400044', '11630037', '11327632', '17583583', '10055192', '10641766', '10430586', '13195752'
 
Tracing Decease case
====================
select *   from  utmc_compositional_transactions 
where  Report_Date <= '2016-09-01' and Transaction_Code = 'TO'
order by Date_Of_Transaction




select *   from  utmc_member_information 
where  Report_Date >= '2010-09-01' and 
EPF_No in 
('10402564', '61272095', '17532738', '15451436', '05428762', '13581257', '16028600', '04159001', '10680005', '15286233', '13621082', '14901511', '10142244', '13825195')






update  utmc_member_information 
set Adjustment = 'S' 
where Report_Date >= '2010-09-01' and 
EPF_No in 
('10402564', '61272095', '17532738', '15451436', '05428762', '13581257', '16028600', '04159001', '10680005', '15286233', '13621082', '14901511', '10142244', '13825195')








2 - NOT HOLDER_NO IN ('9504', '19765') ==> "backdate to cover 3733"
3 - FUND 06 - 2016/07/31 USED 0.2021
4 - patch to rpt 12 last day market value from rpt 11 of the sum(member market value)
5 - PATCH RPT 11, RPT 15, RPT 3 ONLY DISPLAY CURRENT MONTH ACTIVE MEMBER
6 - Patch RPT 10 only display current active member (exclude release member)
7 - PATCH RPT 13 DISPLASE CURRENT ACTIVE MEMBER
8 - PATCH ELIMINATE PENDING STATUS FOR DAILY FUND NAV
9 - CHANGE XD TO XV
10 - patch to rpt 18 last day market value from rpt 11 of the sum(member market value)
11 - patch to rpt 3 & rpt 11 and rpt 15 last day market value from rpt 11 of the sum(member market value)
12 - patch rpt 10 ==> rpt 3  to display reversal member...
		
INSERT INTO eppa_wbr1.utmc_member_information (Code, Passport_No, EPF_No, NIC, NIC_Date, Birthdate, Gender, Effective_Date_Of_Report, Report_Date, Adjustment, IPD_Member_Acc_No)

			SELECT Code, Passport_No, EPF_No, NIC, NIC_Date, Birthdate, Gender, '2012-08-31', '2012-08-31', 'T', IPD_Member_Acc_No  
			FROM eppa_wbr1.utmc_member_information WHERE EPF_No IN 
				
(
SELECT B.Member_EPF_No FROM eppa_wbr1.utmc_compositional_transactions B 
				 WHERE B.Report_Date = '2012-08-31'
AND NOT B.Member_EPF_No IN 

					(SELECT EPF_No FROM eppa_wbr1.utmc_member_information A WHERE A.Report_Date = '2012-08-31')

			GROUP BY B.Member_EPF_No order by ID DESC
) 
		
group by EPF_No;



Feedback
--------
1 - rpt 12 service charge display in last day of the month irregards working day or holiday
2 - rpt 11, rpt 15, rpt 3 not control by rpt 10 div member (resolved)
 
Add Info
--------
DROP USER UTS CASCADE;

CREATE USER UTS
  IDENTIFIED BY "UTS"
  DEFAULT TABLESPACE USERS
  TEMPORARY TABLESPACE TEMP
  PROFILE DEFAULT
  ACCOUNT UNLOCK;
  -- 3 Roles for UTS 
  GRANT CONNECT TO UTS WITH ADMIN OPTION;
  GRANT DBA TO UTS WITH ADMIN OPTION;
  GRANT RESOURCE TO UTS;
  ALTER USER UTS DEFAULT ROLE ALL;
  -- 5 System Privileges for UTS 
  GRANT DELETE ANY TABLE TO UTS WITH ADMIN OPTION;
  GRANT INSERT ANY TABLE TO UTS WITH ADMIN OPTION;
  GRANT SELECT ANY TABLE TO UTS WITH ADMIN OPTION;
  GRANT UNLIMITED TABLESPACE TO UTS WITH ADMIN OPTION;
  GRANT UPDATE ANY TABLE TO UTS WITH ADMIN OPTION;

Import
------
	imp "system/abcd1234" file=C:\Apex_Database_backup_sep\Nov16data\uts.dmp log=C:\Apex_Database_backup_sep\Nov16data\uts.log fromuser=uts touser=uts ignore=y rows=y indexes=y constraints=y
	imp "system/abcd1234" file=C:\Apex_Database_backup_sep\Nov16data\uts.dmp log=C:\Apex_Database_backup_sep\Nov16data\uts.log fromuser=uts touser=uts ignore=y rows=y indexes=n constraints=n


 show variables like 'secure%';
 
 change my.ini in c:/programdata/mysql5.7

                                    
