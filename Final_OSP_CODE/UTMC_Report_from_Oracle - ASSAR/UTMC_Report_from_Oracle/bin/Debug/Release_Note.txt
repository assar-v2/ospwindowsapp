Process Sequence
================
1==	Import fund level report with following sequence
		==> rpt 16, 18, 12, 17
	Import member level report with following sequence
		==> rpt 15, 11, 10, 14, 13, 03
2==	Export UTS UTMC rpt (03, 10, 11, 12, 13, 14, 15, 16, 17, && 18) to apex_export folder
3==	Copy UTS UTMC rpt to OSP folder
	Import UTMC 17 rpt && rpt 2


10.10.10.141 oracle kl sentral
192.168.0.109 enjoy08



select * from utmc_fund_asset_class_composition
where  

select * from utmc_fund_sector_composition
select * from 

utmc_fund_shariah_composition
select * from 

utmc_fund_country_composition


select * from 

utmc_fund_currency_composition



fund level
----------

Select * from utmc_fund_daily_nav where Report_Date = '2016-09-30';


Select * from utmc_daily_nav_fund where Report_Date = '2016-09-30';


Select * from utmc_daily_nav where Report_Date = '2016-09-30';


Select * from utmc_fund_corporate_actions where Report_Date = '2016-09-30';




delete from utmc_fund_asset_class_composition;



delete from utmc_fund_sector_composition;


Delete from utmc_fund_shariah_composition;


Delete from utmc_fund_country_composition;


Delete from utmc_fund_currency_composition;


member level
------------

Select * from utmc_member_information where Report_Date = '2016-09-30';

Select * from utmc_compositional_transactions where Report_Date = '2016-09-30';

Select * from utmc_compositional_investment where Report_Date = '2016-09-30';

Select * from utmc_transferred where Report_Date = '2016-09-30';

Select * from utmc_release_control where Report_Date = '2016-09-30';

Select * from utmc_member_investment where Report_Date = '2016-09-30';



Adjustment (Hardcode)
----------
11667355 adjust to ==> "S"
NOT HOLDER_NO IN ('9504', '19765') ==> "BackDate to cover 3733"
FUND 06 - 2016/07/31 USED 0.2021
				    if (End_date == "2016/07/31")
                                    {
                                        if (IPD_Fund_Code == "06")
                                        {

                                            decimal Market_Value_TT = 0.00M;
                                            decimal Units_TT = Convert.ToDecimal(Units.ToString());
                                            Market_Value_TT = (Units_TT * Convert.ToDecimal("0.2021".ToString()));
                                            Market_Value_TT = decimal.Round(Market_Value_TT, 4);
                                            Market_Value = Convert.ToString(Market_Value_TT.ToString());

                                        }
patch to rpt 12 last day market value from rpt 11 of the sum(member market value)
                                    }
