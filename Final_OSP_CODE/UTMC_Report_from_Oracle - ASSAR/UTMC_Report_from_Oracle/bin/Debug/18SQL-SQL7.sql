
DELETE   from  utmc_member_information where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_compositional_transactions where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_compositional_investment where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_transferred where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_release_control where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_member_investment where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';

DELETE   from  utmc_fund_daily_nav where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_daily_nav_fund where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_daily_nav where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_fund_corporate_actions where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';


DELETE   from  utmc_fund_asset_class_composition where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_fund_sector_composition where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_fund_shariah_composition where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_fund_country_composition where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';
DELETE   from  utmc_fund_currency_composition where  Report_Date >= '2016-11-01'   AND Report_Date <= '2016-11-30';





